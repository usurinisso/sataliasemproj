package ktu.offloadz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OffloadzApplication {

  public static void main(String[] args) {
    SpringApplication.run(OffloadzApplication.class, args);
  }
}
