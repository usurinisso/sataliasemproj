package ktu.offloadz.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Autowired private BCryptPasswordEncoder bCryptPasswordEncoder;

  @Autowired private DataSource dataSource;

  @Autowired AuthenticationSuccessHandler successHandler;

  @Value("${spring.queries.users-query}")
  private String usersQuery;

  @Value("${spring.queries.roles-query}")
  private String rolesQuery;

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.jdbcAuthentication()
        .usersByUsernameQuery(usersQuery)
        .authoritiesByUsernameQuery(rolesQuery)
        .dataSource(dataSource)
        .passwordEncoder(bCryptPasswordEncoder);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {

    http.authorizeRequests()
        .antMatchers("/")
        .permitAll()
        .antMatchers("/login")
        .permitAll()
        .antMatchers("/registration")
        .permitAll()
        .antMatchers("/confirm-account")
        .permitAll()
        .antMatchers("/reset-password")
        .permitAll()
        .antMatchers("/confirm-reset")
        .permitAll()
        .antMatchers("/change-password")
        .permitAll()
        .antMatchers("/dispatcher/**")
        .hasAnyAuthority("DISPATCHER","ADMIN")
        .antMatchers("/courier/**")
        .hasAnyAuthority("COURIER","ADMIN")
        .antMatchers("/admin/**")
        .hasAuthority("ADMIN")
        .anyRequest()
        .authenticated()
        .and()
        .csrf()
        .disable()
        .formLogin()
        .loginPage("/login")
        .failureUrl("/login?error=true")
        .successHandler(successHandler)
        // .defaultSuccessUrl("/admin/adminHome")

        .usernameParameter("email")
        .passwordParameter("password")
        /*.antMatchers("/user/**").hasAuthority("USER").anyRequest()
        .authenticated().and().csrf().disable().formLogin()
        .loginPage("/login").failureUrl("/login?error=true")
        .defaultSuccessUrl("/user/userHome")
        .usernameParameter("email")
        .passwordParameter("password")*/
        .and()
        .logout()
        .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
        .logoutSuccessUrl("/")
        .and()
        .exceptionHandling()
        .accessDeniedPage("/access-denied");
  }

  @Override
  public void configure(WebSecurity web) throws Exception {

    web.ignoring()
        .antMatchers(
            "/resources/**", "/static/**", "/css/**", "/js/**", "/images/**", "/console/**");
  }
}
