package ktu.offloadz.controller;

import ktu.offloadz.model.Comment;
import ktu.offloadz.model.Order;
import ktu.offloadz.model.User;
import ktu.offloadz.service.OrderService;
import ktu.offloadz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Controller
public class CommentController {
  @Autowired private OrderService orderService;
  @Autowired private UserService userService;

  @RequestMapping(value = "/comment/view/{id}/", method = RequestMethod.GET)
  public ModelAndView viewOrderComments(@PathVariable(name = "id") int id) {
    Order order = orderService.findById(id);
    Set<Comment> comments = order.getOrderComment();
    int n = comments.size();
    List<Comment> commentList = new ArrayList<Comment>(n);
    for (Comment i : comments) {
      commentList.add(i);
    }
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    User user = userService.findUserByEmail(auth.getName());
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject("order", order);
    modelAndView.addObject("role", user.getRole());
    modelAndView.addObject("comments", commentList);
    modelAndView.setViewName("comment/view");
    return modelAndView;
  }

  @RequestMapping(value = "comment/add/{id}/", method = RequestMethod.GET)
  public ModelAndView addComment(@PathVariable(name = "id") int id, ModelAndView modelAndView) {
    Order order = orderService.findById(id);
    modelAndView.addObject("order", order);
    modelAndView.addObject("comment", new Comment());
    modelAndView.setViewName("/comment/add");
    return modelAndView;
  }

  @RequestMapping(value = "comment/save/{id}/", method = RequestMethod.POST)
  public ModelAndView saveComment(
      @Valid int id, @Valid Comment comment, ModelAndView modelAndView) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    modelAndView.addObject("comment", comment);
    User user = userService.findUserByEmail(auth.getName());
    Order order = orderService.findById(id);
    orderService.attachComment(user, order, comment.getMessage());
    modelAndView.addObject("order", order);
    modelAndView.addObject("users", orderService.findAll());
    modelAndView.setViewName("redirect:/comment/view/{id}/");
    return modelAndView;
  }
}
