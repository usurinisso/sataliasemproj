package ktu.offloadz.controller;

import ktu.offloadz.model.Order;
import ktu.offloadz.model.User;
import ktu.offloadz.service.OrderService;
import ktu.offloadz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class OrderController {

  @Autowired private UserService userService;

  @Autowired private OrderService orderService;

  @RequestMapping(value = "/dispatcher/order/all", method = RequestMethod.GET)
  public ModelAndView allAsDispatcherOrders() {
    ModelAndView modelAndView = new ModelAndView();
    List<Order> list = orderService.findAll();
    modelAndView.addObject("orderList", list);
    modelAndView.addObject("titleName", "All orders");
    modelAndView.addObject("displayType", "all");
    return modelAndView;
  }

  @RequestMapping(value = "/dispatcher/order/selected", method = RequestMethod.GET)
  public ModelAndView selectedDispatcherOrders() {
    ModelAndView modelAndView = new ModelAndView();
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    modelAndView.setViewName("/dispatcher/order/all");
    User user = userService.findUserByEmail(auth.getName());
    List<Order> list = orderService.findSelectedDispatcher(user.getName());
    modelAndView.addObject("orderList", list);
    modelAndView.addObject("titleName", "Your orders");
    modelAndView.addObject("displayType", "selected");
    return modelAndView;
  }

  @RequestMapping(value = "/courier/order/all", method = RequestMethod.GET)
  public ModelAndView allAsCourierOrders() {
    ModelAndView modelAndView = new ModelAndView();
    List<Order> list = orderService.findAll();
    modelAndView.addObject("orderList", list);
    return modelAndView;
  }

  @RequestMapping(value = "/courier/order/update/{id}", method = RequestMethod.GET)
  public ModelAndView updateOrderForm(@PathVariable(name = "id") int id) {
    Order order = orderService.findById(id);
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.addObject("order", order);
    modelAndView.setViewName("courier/order/update");
    return modelAndView;
  }

  @RequestMapping(
      value = "/courier/order/update/{id}",
      params = "update",
      method = RequestMethod.POST)
  public ModelAndView updateOrder(
      @RequestParam(name = "id", required = true) int id,
      @RequestParam(name = "orderState", required = true) int orderState) {
    ModelAndView modelAndView = new ModelAndView();
    Order order = orderService.findById(id);
    orderService.updateState(order, orderState);
    modelAndView.setViewName("redirect:/courier/order/all");
    return modelAndView;
  }

  @RequestMapping(
      value = "/courier/order/update/{id}",
      params = "cancel",
      method = RequestMethod.POST)
  public ModelAndView cancelUpdateOrder() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("redirect:/courier/order/all");
    return modelAndView;
  }

  @RequestMapping(value = "dispatcher/order/create", method = RequestMethod.GET)
  public ModelAndView createOrder(ModelAndView modelAndView) {
    modelAndView.addObject(new Order());
    modelAndView.setViewName("/dispatcher/order/create");
    return modelAndView;
  }

  @PostMapping(value = "/dispatcher/order/save")
  public ModelAndView saveOrder(
      @Valid Order order, BindingResult result, ModelAndView modelAndView) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    User user = userService.findUserByEmail(auth.getName());
    order.setCreated_order(user.getName());
    orderService.save(order);
    modelAndView.addObject("users", orderService.findAll());
    modelAndView.setViewName("redirect:/dispatcher/order/all");
    return modelAndView;
  }

  @RequestMapping(value = "dispatcher/order/filter", method = RequestMethod.GET)
  public ModelAndView filterOrders(ModelAndView modelAndView) {
    Integer id = 0, state = 0;
    Double minWeight = 0.0,
        maxWeight = 0.0,
        minHeight = 0.0,
        maxHeight = 0.0,
        minWidth = 0.0,
        maxWidth = 0.0,
        minLength = 0.0,
        maxLength = 0.0;
    String origin = "", dest = "";
    modelAndView.addObject("id", id);
    modelAndView.addObject("minWeight", minWeight);
    modelAndView.addObject("maxWeight", maxWeight);
    modelAndView.addObject("minHeight", minHeight);
    modelAndView.addObject("maxHeight", maxHeight);
    modelAndView.addObject("minWidth", minWidth);
    modelAndView.addObject("maxWidth", maxWidth);
    modelAndView.addObject("minLength", minLength);
    modelAndView.addObject("maxLength", maxLength);
    modelAndView.addObject("origin", origin);
    modelAndView.addObject("dest", dest);
    modelAndView.addObject("state", state);
    modelAndView.setViewName("/dispatcher/order/filter");
    return modelAndView;
  }

  @PostMapping(value = "/dispatcher/order/filtered")
  public ModelAndView showFilteredOrders(
      Integer id,
      Double minWeight,
      Double maxWeight,
      Double minHeight,
      Double maxHeight,
      Double minWidth,
      Double maxWidth,
      Double minLength,
      Double maxLength,
      String origin,
      String dest,
      Integer state,
      ModelAndView modelAndView) {
    modelAndView.setViewName("/dispatcher/order/all");
    List<Order> filteredOrders = new ArrayList<Order>();
    filteredOrders =
        orderService.filter(
            id, minWeight, maxWeight, minHeight, maxHeight, minWidth, maxWidth, minLength,
            maxLength, origin, dest, state);
    modelAndView.addObject("orderList", filteredOrders);
    modelAndView.addObject("titleName", "Filtered orders");
    modelAndView.addObject("displayType", "filtered");
    return modelAndView;
  }
}
