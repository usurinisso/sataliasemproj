package ktu.offloadz.controller.User;

import ktu.offloadz.model.User;
import ktu.offloadz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class AdminController {

  @Autowired private UserService userService;

  @RequestMapping(value = "/admin/adminHome", method = RequestMethod.GET)
  public ModelAndView home() {
    ModelAndView modelAndView = new ModelAndView();
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    User user = userService.findUserByEmail(auth.getName());
    modelAndView.addObject(
        "userName",
        "Welcome " + user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")");
    modelAndView.setViewName("admin/adminHome");
    return modelAndView;
  }

  @RequestMapping(value = "/admin/user-list", method = RequestMethod.GET)
  public ModelAndView userList(@RequestParam(defaultValue="1") int page) {
    ModelAndView modelAndView = new ModelAndView();
    User user = new User();
    PageRequest pageable = PageRequest.of(page - 1, 4);
    Page<User> userPage = userService.findAll(pageable);
    int totalPages = userPage.getTotalPages();
    if(totalPages > 0) {
      List<Integer> pageNumbers = IntStream.rangeClosed(1,totalPages).boxed().collect( Collectors.toList());
      modelAndView.addObject("pageNumbers", pageNumbers);
    }
    modelAndView.addObject ("currentPage", page);
    modelAndView.addObject ( "totalPages", totalPages);
    modelAndView.addObject("users", userPage.getContent());
    modelAndView.addObject("user", user);
    modelAndView.setViewName("admin/user-list");
    return modelAndView;
  }

  @RequestMapping(value = "/admin/save-user", method = RequestMethod.POST)
  public ModelAndView saveUser(
          @Valid User user,
          @RequestParam(name = "role", required = true) String role,
          BindingResult bindingResult,
          RedirectAttributes redirectAttributes) {
    ModelAndView modelAndView = new ModelAndView();
    User userExists = userService.findUserByEmail(user.getEmail());
    if (userExists != null) {
      bindingResult.rejectValue(
              "email", "error.user", "There is already a user registered with the email provided");
    }
    if (bindingResult.hasErrors()) {
    } else {
      user.setActive(1);
      userService.saveUser(user, role);
      modelAndView.setViewName("redirect:/admin/user-list");
    }
    return modelAndView;
  }

  @RequestMapping(value = "/admin/edit-user", method = RequestMethod.POST)
  public ModelAndView editUser(
          @Valid User user,
          @RequestParam(name = "role", required = true) String role,
          BindingResult bindingResult,
          RedirectAttributes redirectAttributes) {
    ModelAndView modelAndView = new ModelAndView();
    if (bindingResult.hasErrors()) {
    } else {
      user.setActive(1);
      userService.editUser(user, role);
      modelAndView.setViewName("redirect:/admin/user-list");
    }
    return modelAndView;
  }

  @RequestMapping(value = "/admin/findUserById", method = RequestMethod.GET)
  @ResponseBody
  public  User findUserById(int id) {
    return userService.findUserById (id);
  }

  @RequestMapping(value = "/admin/delete-user", method = RequestMethod.GET)
  public ModelAndView deleteUser(@RequestParam(value = "id") int id) {
    ModelAndView modelAndView = new ModelAndView();
    userService.deleteUserById(id);
    modelAndView.setViewName("redirect:/admin/user-list");
    return modelAndView;
  }
}
