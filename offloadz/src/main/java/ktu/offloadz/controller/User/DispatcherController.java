package ktu.offloadz.controller.User;

import ktu.offloadz.model.User;
import ktu.offloadz.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DispatcherController {

  @Autowired private UserService userService;

  @RequestMapping(value = "/dispatcher/dispatcherHome", method = RequestMethod.GET)
  public ModelAndView user() {
    ModelAndView modelAndView = new ModelAndView();
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    User user = userService.findUserByEmail(auth.getName());
    if (user.getRole() == "ADMIN") {
      modelAndView.addObject(
              "userName",
              "Welcome " + user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")");
      modelAndView.setViewName("redirect:/admin/adminHome");
    }
    else {
      modelAndView.addObject ( "userName", ("Welcome " + user.getName ()) + "!" );
      modelAndView.addObject ( "userMessage", "Your role is dispatcher." );
      modelAndView.setViewName ( "dispatcher/dispatcherHome" );
    }
    return modelAndView;
  }
}
