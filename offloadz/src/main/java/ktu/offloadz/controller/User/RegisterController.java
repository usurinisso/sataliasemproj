package ktu.offloadz.controller.User;

import ktu.offloadz.model.User;
import ktu.offloadz.model.VerificationToken;
import ktu.offloadz.service.CaptchaService;
import ktu.offloadz.service.UserService;
import ktu.offloadz.service.VerificationEmailService;
import ktu.offloadz.service.VerificationTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class RegisterController {

  @Autowired private UserService userService;
  @Autowired private VerificationEmailService verificationEmailService;
  @Autowired private VerificationTokenService verificationTokenService;
  @Autowired private CaptchaService captchaService;

  // Redirect to registration page

  @RequestMapping(value = "/registration", method = RequestMethod.GET)
  public ModelAndView registration() {
    ModelAndView modelAndView = new ModelAndView();
    User user = new User();
    String confirmPassword = "";
    modelAndView.addObject("user", user);
    modelAndView.addObject("confirmPassword", confirmPassword);
    modelAndView.setViewName("registration");
    return modelAndView;
  }

  // Creating new user

  @RequestMapping(value = "/registration", method = RequestMethod.POST)
  public ModelAndView createNewUser(
      @Valid User user,
      BindingResult bindingResult,
      @RequestParam(name = "role", required = true) String role,
      @RequestParam(name = "g-recaptcha-response") String recaptchaResponse) {
    ModelAndView modelAndView = new ModelAndView();
    boolean err = false;
    try {
      captchaService.processResponse(recaptchaResponse);
    } catch (Exception e) {
      modelAndView.addObject("errormsg", "Please confirm captcha to register");
      err = true;
    }
    User userExists = userService.findUserByEmail(user.getEmail());
    if (userExists != null) {
      bindingResult.rejectValue(
          "email", "error.user", "There is already a user registered with the email provided");
    }
    if (!user.getPassword().equals(user.getConfirmPassword())) {
      bindingResult.rejectValue(
          "confirmPassword", "error.confirmPassword", "Passwords do not match.");
      modelAndView.setViewName("registration");
    }
    if (bindingResult.hasErrors() || err) {
      modelAndView.setViewName("registration");
    } else {
      user.setConfirmPassword("");
      user.setActive(0);
      userService.saveUser(user, role);

      verificationEmailService.sendConfirmationEmail(user);

      confirmationMessage(modelAndView, user);

      modelAndView.setViewName("registration");
    }
    return modelAndView;
  }

  // Confirmation screen

  @RequestMapping(
      value = "/confirm-account",
      method = {RequestMethod.GET, RequestMethod.POST})
  public ModelAndView confirmUserAccount(
      ModelAndView modelAndView, @RequestParam("token") String verificationToken) {
    VerificationToken token = verificationTokenService.findByVerificationToken(verificationToken);

    if (token != null) {
      User user = userService.findUserByEmail(token.getUser().getEmail());
      userService.updateUserStatus(user, 1);
      authWithoutPassword(user);
      String role = user.getRole();
      if (role.equals("COURIER")) modelAndView.setViewName("redirect:/courier/courierHome");
      else if (role.equals("DISPATCHER"))
        modelAndView.setViewName("redirect:/dispatcher/dispatcherHome");
    } else {
      modelAndView.addObject("message", "The verification link is expired or does not exist!");
      modelAndView.setViewName("verification-error");
    }

    return modelAndView;
  }

  public void authWithoutPassword(User user) {
    List<GrantedAuthority> authorities = new ArrayList<>();
    authorities.add(new SimpleGrantedAuthority(user.getRole()));
    Authentication auth =
        new UsernamePasswordAuthenticationToken(user.getEmail(), null, authorities);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }

  // adds confirmation message text to the page

  public void confirmationMessage(ModelAndView modelAndView, User user) {
    modelAndView.addObject(
        "successMessage",
        "User has been registered successfully. "
            + "A confirmation email has been sent to "
            + user.getEmail());
    modelAndView.addObject("user", new User());
  }
}
