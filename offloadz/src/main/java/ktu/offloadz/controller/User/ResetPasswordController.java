package ktu.offloadz.controller.User;

import ktu.offloadz.model.User;
import ktu.offloadz.model.VerificationToken;
import ktu.offloadz.service.CaptchaService;
import ktu.offloadz.service.UserService;
import ktu.offloadz.service.VerificationEmailService;
import ktu.offloadz.service.VerificationTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ResetPasswordController {

  @Autowired private VerificationTokenService verificationTokenService;
  @Autowired private VerificationEmailService verificationEmailService;

  @Autowired private CaptchaService captchaService;

  @Autowired private UserService userService;

  @RequestMapping(value = "/reset-password", method = RequestMethod.GET)
  public ModelAndView resetPassword(ModelAndView modelAndView, User user) {
    modelAndView.addObject("user", user);
    modelAndView.setViewName("reset-password");
    return modelAndView;
  }

  @RequestMapping(value = "/reset-password", method = RequestMethod.POST)
  public ModelAndView recoverPassword(
      ModelAndView modelAndView,
      User user,
      @RequestParam(name = "g-recaptcha-response") String recaptchaResponse) {
    User existingUser = userService.findUserByEmail(user.getEmail());
    boolean err = false;
    try {
      captchaService.processResponse(recaptchaResponse);
    } catch (Exception e) {
      modelAndView.addObject("errormsg", "Please confirm captcha to reset password");
      err = true;
    }
    if (existingUser != null && !err) {
      // Create token

      verificationEmailService.sendPasswordResetEmail(existingUser);

      modelAndView.addObject(
          "message", "Request to reset password received. Check your inbox for the reset link.");
      modelAndView.setViewName("password-reset-success");

    } else if (!err) {
      modelAndView.addObject("message", "This email address does not exist!");
      modelAndView.setViewName("password-reset-error");
    } else {
      modelAndView.setViewName("reset-password");
    }
    return modelAndView;
  }

  @RequestMapping(
      value = "/confirm-reset",
      method = {RequestMethod.GET, RequestMethod.POST})
  public ModelAndView validateResetToken(
      ModelAndView modelAndView, @RequestParam("token") String confirmationToken) {
    VerificationToken token = verificationTokenService.findByVerificationToken(confirmationToken);

    if (token != null) {
      User user = userService.findUserByEmail(token.getUser().getEmail());
      userService.updateUserStatus(user, 1);
      modelAndView.addObject("user", user);
      modelAndView.addObject("email", user.getEmail());
      modelAndView.setViewName("change-password");
    } else {
      modelAndView.addObject("message", "The verification link is expired or does not exist!");
      modelAndView.setViewName("password-reset-error");
    }
    return modelAndView;
  }
  // Endpoint to update a user's password
  @RequestMapping(value = "/change-password", method = RequestMethod.POST)
  public ModelAndView resetUserPassword(
      ModelAndView modelAndView,
      @RequestParam(name = "password") String password,
      @RequestParam(name = "confirmPassword") String confirmPassword,
      User user) {
    if (user.getEmail() != null) {
      if (confirmPassword.equals(password) && password.length() >= 5) {
        // Use email to find user
        User tokenUser = userService.findUserByEmail(user.getEmail());
        userService.updateUserPassword(tokenUser, password);
        modelAndView.addObject(
            "message", "Password successfully reset. You can now log in with the new credentials.");
        modelAndView.setViewName("password-reset-success");
      } else if (password.length() < 5) {
        modelAndView.addObject("user", user);
        modelAndView.addObject("email", user.getEmail());
        modelAndView.addObject("message", "*Your password must have at least 5 characters");
        modelAndView.setViewName("change-password");
      } else {
        modelAndView.addObject("user", user);
        modelAndView.addObject("email", user.getEmail());
        modelAndView.addObject("message", "*Passwords do not match!");
        modelAndView.setViewName("change-password");
      }
    } else {
      modelAndView.addObject("message", "The link is invalid or broken!");
      modelAndView.setViewName("password-reset-error");
    }
    return modelAndView;
  }
}
