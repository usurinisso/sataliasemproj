/*

package ktu.offloadz.controller;

import ktu.offloadz.model.Role;
import ktu.offloadz.model.User;
import ktu.offloadz.model.VerificationToken;
import ktu.offloadz.service.UserService;
import ktu.offloadz.service.VerificationEmailService;
import ktu.offloadz.service.VerificationTokenService;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

*/
/*

@Controller
@Validated
public class UserController {

  @Autowired private UserService userService;
  @Autowired private VerificationTokenService verificationTokenService;
  @Autowired private VerificationEmailService verificationEmailService;

  @RequestMapping(
      value = {"/", "/login"},
      method = RequestMethod.GET)
  public ModelAndView login() {
    ModelAndView modelAndView = new ModelAndView();
    modelAndView.setViewName("login");
    return modelAndView;
  }

  @RequestMapping(value = "/registration", method = RequestMethod.GET)
  public ModelAndView registration() {
    ModelAndView modelAndView = new ModelAndView();
    User user = new User();
    modelAndView.addObject("user", user);
    modelAndView.setViewName("registration");
    return modelAndView;
  }

  @RequestMapping(value = "/registration", method = RequestMethod.POST)
  public ModelAndView createNewUser(
      @Valid User user,
      BindingResult bindingResult,
      @RequestParam(name = "role", required = true) String role) {
    ModelAndView modelAndView = new ModelAndView();
    User userExists = userService.findUserByEmail(user.getEmail());
    if (userExists != null) {
      bindingResult.rejectValue(
          "email", "error.user", "There is already a user registered with the email provided");
    }
    if (bindingResult.hasErrors()) {
      modelAndView.setViewName("registration");
    } else {
      user.setActive(0);
      userService.saveUser(user, role);
      VerificationToken verificationToken = new VerificationToken(user);
      verificationTokenService.saveVerificationToken(verificationToken);

      SimpleMailMessage mailMessage = new SimpleMailMessage();
      mailMessage.setTo(user.getEmail());
      mailMessage.setSubject("Complete Registration!");
      mailMessage.setFrom("example@gmail.com");
      mailMessage.setText(
          "To confirm your account, please click here : "
              + "http://localhost:8081/confirm-account?token="
              + verificationToken.getVerificationToken());

      verificationEmailService.sendEmail(mailMessage);

      modelAndView.addObject(
          "successMessage",
          "User has been registered successfully. "
              + "A confirmation email has been sent to "
              + user.getEmail());
      modelAndView.addObject("user", new User());
      modelAndView.setViewName("registration");
    }
    return modelAndView;
  }

  @RequestMapping(
      value = "/confirm-account",
      method = {RequestMethod.GET, RequestMethod.POST})
  public ModelAndView confirmUserAccount(
      ModelAndView modelAndView, @RequestParam("token") String verificationToken) {
    VerificationToken token = verificationTokenService.findByVerificationToken(verificationToken);

    if (token != null) {
      User user = userService.findUserByEmail(token.getUser().getEmail());
      userService.updateUserStatus(user, 1);
      authWithoutPassword(user);
      modelAndView.setViewName("redirect:/user/userHome");
    } else {
      modelAndView.addObject("message", "The verification link is expired or does not exist!");
      modelAndView.setViewName("verification-error");
    }

    return modelAndView;
  }

  @RequestMapping(value = "/reset-password", method = RequestMethod.GET)
  public ModelAndView resetPassword(ModelAndView modelAndView, User user) {
    modelAndView.addObject("user", user);
    modelAndView.setViewName("reset-password");
    return modelAndView;
  }

  @RequestMapping(value = "/reset-password", method = RequestMethod.POST)
  public ModelAndView recoverPassword(ModelAndView modelAndView, User user) {
    User existingUser = userService.findUserByEmail(user.getEmail());
    if (existingUser != null) {
      // Create token
      VerificationToken confirmationToken = new VerificationToken(existingUser);

      verificationTokenService.saveVerificationToken(confirmationToken);

      SimpleMailMessage mailMessage = new SimpleMailMessage();
      mailMessage.setTo(existingUser.getEmail());
      mailMessage.setSubject("Complete Password Reset!");
      mailMessage.setFrom("Offloadz123@gmail.com");
      mailMessage.setText(
          "To complete the password reset process, please click here: "
              + "http://localhost:8081/confirm-reset?token="
              + confirmationToken.getVerificationToken());

      verificationEmailService.sendEmail(mailMessage);

      modelAndView.addObject(
          "message", "Request to reset password received. Check your inbox for the reset link.");
      modelAndView.setViewName("password-reset-success");

    } else {
      modelAndView.addObject("message", "This email address does not exist!");
      modelAndView.setViewName("password-reset-error");
    }
    return modelAndView;
  }

  @RequestMapping(
      value = "/confirm-reset",
      method = {RequestMethod.GET, RequestMethod.POST})
  public ModelAndView validateResetToken(
      ModelAndView modelAndView, @RequestParam("token") String confirmationToken) {
    VerificationToken token = verificationTokenService.findByVerificationToken(confirmationToken);

    if (token != null) {
      User user = userService.findUserByEmail(token.getUser().getEmail());
      userService.updateUserStatus(user, 1);
      modelAndView.addObject("user", user);
      modelAndView.setViewName("change-password");
    } else {
      modelAndView.addObject("message", "The verification link is expired or does not exist!");
      modelAndView.setViewName("password-reset-error");
    }
    return modelAndView;
  }

  // Endpoint to update a user's password
  @RequestMapping(value = "/change-password", method = RequestMethod.POST)
  public ModelAndView resetUserPassword(
          ModelAndView modelAndView, @RequestParam(name = "password", required = true)
          /*@Length(min = 5, message = "*Your password must have at least 5 characters") String password, Errors errors, User user) { //NEED TO FIX PASSWORD RESET VALIDATION
    if(errors.hasErrors()) {
      modelAndView.setViewName("change-password");
    }
      if (user.getEmail() != null) {
        // Use email to find user
        User tokenUser = userService.findUserByEmail(user.getEmail());
        userService.updateUserPassword(tokenUser, password);
        modelAndView.addObject(
                "message", "Password successfully reset. You can now log in with the new credentials.");
        modelAndView.setViewName("password-reset-success");
      } else {
        modelAndView.addObject("message", "The link is invalid or broken!");
        modelAndView.setViewName("password-reset-error");
      }
    return modelAndView;
  }

  @RequestMapping(value = "/admin/adminHome", method = RequestMethod.GET)
  public ModelAndView home() {
    ModelAndView modelAndView = new ModelAndView();
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    User user = userService.findUserByEmail(auth.getName());
    modelAndView.addObject(
        "userName",
        "Welcome " + user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")");
    modelAndView.addObject("adminMessage", "This Page is available to Users with Admin Role");
    modelAndView.setViewName("admin/adminHome");
    return modelAndView;
  }

  @RequestMapping(value = "/admin/user-list", method = RequestMethod.GET)
  public ModelAndView userList(@RequestParam(defaultValue="1") int page) {
    ModelAndView modelAndView = new ModelAndView();
    User user = new User();
    PageRequest pageable = PageRequest.of(page - 1, 4);
    Page<User> userPage = userService.findAll(pageable);
    int totalPages = userPage.getTotalPages();
    if(totalPages > 0) {
      List<Integer> pageNumbers = IntStream.rangeClosed(1,totalPages).boxed().collect( Collectors.toList());
      modelAndView.addObject("pageNumbers", pageNumbers);
    }
    modelAndView.addObject ("currentPage", page);
    modelAndView.addObject ( "totalPages", totalPages);
    modelAndView.addObject("users", userPage.getContent());
    modelAndView.addObject("user", user);
    modelAndView.setViewName("admin/user-list");
    return modelAndView;
  }

  @RequestMapping(value = "/admin/save-user", method = RequestMethod.POST)
  public ModelAndView saveUser(
          @Valid User user,
          @RequestParam(name = "role", required = true) String role,
          BindingResult bindingResult,
          RedirectAttributes redirectAttributes) {
    ModelAndView modelAndView = new ModelAndView();
    User userExists = userService.findUserByEmail(user.getEmail());
    if (userExists != null) {
      bindingResult.rejectValue(
              "email", "error.user", "There is already a user registered with the email provided");
    }
    if (bindingResult.hasErrors()) {
    } else {
      user.setActive(1);
      userService.saveUser(user, role);
      modelAndView.setViewName("redirect:/admin/user-list");
    }
    return modelAndView;
  }

    @RequestMapping(value = "/admin/edit-user", method = RequestMethod.POST)
    public ModelAndView editUser(
            @Valid User user,
            @RequestParam(name = "role", required = true) String role,
            BindingResult bindingResult,
            RedirectAttributes redirectAttributes) {
        ModelAndView modelAndView = new ModelAndView();
        if (bindingResult.hasErrors()) {
        } else {
            user.setActive(1);
            userService.editUser(user, role);
            modelAndView.setViewName("redirect:/admin/user-list");
        }
        return modelAndView;
    }

  @RequestMapping(value = "/admin/findUserById", method = RequestMethod.GET)
  @ResponseBody
  public  User findUserById(int id) {
    return userService.findUserById (id);
  }

  @RequestMapping(value = "/admin/delete-user", method = RequestMethod.GET)
  public ModelAndView deleteUser(@RequestParam(value = "id") int id) {
    ModelAndView modelAndView = new ModelAndView();
    userService.deleteUserById(id);
    modelAndView.setViewName("redirect:/admin/user-list");
    return modelAndView;
  }

  @RequestMapping(value = "/user/userHome", method = RequestMethod.GET)
  public ModelAndView user() {
    ModelAndView modelAndView = new ModelAndView();
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    User user = userService.findUserByEmail(auth.getName());
    modelAndView.addObject("userName", ("Welcome " + user.getName()) + "!");
    modelAndView.addObject("userMessage", "Your role is user.");
    modelAndView.setViewName("user/userHome");
    return modelAndView;
  }

  public void authWithoutPassword(User user) {
    List<GrantedAuthority> authorities = new ArrayList<>();
    for (Role role : user.getRoles()) {
      authorities.add(new SimpleGrantedAuthority(role.getRole()));
    }
    Authentication auth =
        new UsernamePasswordAuthenticationToken(user.getEmail(), null, authorities);
    SecurityContextHolder.getContext().setAuthentication(auth);
  }
}

*/
