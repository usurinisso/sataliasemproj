package ktu.offloadz.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "ORDER_TABLE")
public class Order {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ORDER_ID")
  private int orderId;

  @Column(name = "WEIGHT")
  @Positive
  @NotNull(message = "*Please provide the weight.")
  private double weight;

  @Column(name = "HEIGHT")
  @Positive
  @NotNull(message = "*Please provide the height.")
  private double height;

  @Column(name = "WIDTH")
  @Positive
  @NotNull(message = "*Please provide the width.")
  private double width;

  @Column(name = "LENGTH")
  @Positive
  @NotNull(message = "*Please provide the length.")
  private double length;

  @Column(name = "ORIGIN_LOCATION")
  @NotNull(message = "*Please provide the origin location.")
  private String originLocation;

  @Column(name = "ORIGIN_LATITUDE")
  @NotNull(message = "*Please provide the coordinates.")
  private double origin_latitude;

  @Column(name = "ORIGIN_LONGITUDE")
  @NotNull(message = "*Please provide the coordinates.")
  private double origin_longitude;

  @Column(name = "DESTINATION_LATITUDE")
  @NotNull(message = "*Please provide the coordinates.")
  private double destination_latitude;

  @Column(name = "DESTINATION_LONGITUDE")
  @NotNull(message = "*Please provide the coordinates.")
  private double destination_longitude;

  @Column(name = "DESTINATION")
  @NotEmpty(message = "*Please provide the destination.")
  private String destination;

  @Column(name = "CREATED_ORDER")
  private String created_order;

  @OneToOne(targetEntity = OrderState.class, fetch = FetchType.LAZY)
  @JoinColumn(name = "CURRENT_STATE")
  private OrderState orderState;

  @OneToMany(targetEntity = Comment.class, fetch = FetchType.LAZY)
  @JoinColumn(name = "ORDER_COMMENT")
  private Set<Comment> orderComment;
}
