package ktu.offloadz.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "RECIPIENT")
public class Recipient {
  @Id
  @Column(name = "RECIPIENT_ID")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int recipientId;

  @Column(name = "RECIPIENT_FIRST_NAME")
  @NotEmpty(message = "*Please enter the recipient's first name.")
  private String REC_FIRST_NAME;

  @Column(name = "RECIPIENT_LAST_NAME")
  @NotEmpty(message = "*Please enter the recipient's last name.")
  private String author;

  @Column(name = "RECIPIENT_PHONE_NUMBER")
  @NotEmpty(message = "*Please enter the recipient's phone number.")
  private String message;

  @Column(name = "RECIPIENT_EMAIL")
  @Email(message = "*Please provide a valid Email")
  @NotEmpty(message = "*Please enter the recipient's email address.")
  private String email;
}
