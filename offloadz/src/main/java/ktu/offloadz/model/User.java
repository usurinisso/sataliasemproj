package ktu.offloadz.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "USER")
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY) // was AUTO
  @Column(name = "USER_ID")
  private int id;

  @Column(name = "EMAIL")
  @Email(message = "*Please provide a valid Email")
  @NotEmpty(message = "*Please provide an email")
  private String email;

  @Column(name = "PASSWORD")
  @Length(min = 5, message = "*Your password must have at least 5 characters")
  @NotEmpty(message = "*Please provide your password")
  private String password;

  @Transient private String confirmPassword;

  @Column(name = "NAME")
  @NotEmpty(message = "*Please provide your name")
  private String name;

  @Column(name = "LAST_NAME")
  @NotEmpty(message = "*Please provide your last name")
  private String lastName;

  @Column(name = "ACTIVE")
  private int active;

  private static final int EXPIRATION = 60 * 24;

  private Date regExpiryDate;

  @ManyToMany(
      cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
  @JoinTable(
      name = "USER_ROLE",
      joinColumns = @JoinColumn(name = "USER_ID"),
      inverseJoinColumns = @JoinColumn(name = "ROLE_ID"))
  private Set<Role> roles;
  // čia reikės pakoreguot, bet kitaip user liste rodo visą setą rolių nors visada tik vieną turės

  public String getRole() {
    Role role = roles.iterator().next();
    return role.getRole();
  }

  public Date calculateExpiryDate() {
    final Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(new Date().getTime());
    cal.add(Calendar.MINUTE, EXPIRATION);
    return new Date(cal.getTime().getTime());
  }
}
