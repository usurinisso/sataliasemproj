package ktu.offloadz.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@Entity
public class VerificationToken {

  private static final int EXPIRATION = 60 * 24;
  // private static final int EXPIRATION = 2; // testing purposes
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY) // was AUTO
  @Column(name = "TOKEN_ID")
  private long tokenId;

  @Column(name = "VERIFICATION_TOKEN")
  private String verificationToken;

  private Date expiryDate;

  @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
  @JoinColumn(nullable = false, name = "USER_ID")
  private User user;

  private Date calculateExpiryDate(final int expiryTimeInMinutes) {
    final Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(new Date().getTime());
    cal.add(Calendar.MINUTE, expiryTimeInMinutes);
    return new Date(cal.getTime().getTime());
  }

  public VerificationToken(User user) {
    this.user = user;
    expiryDate = calculateExpiryDate(EXPIRATION);
    verificationToken = UUID.randomUUID().toString();
  }
}
