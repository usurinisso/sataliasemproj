package ktu.offloadz.repository;

import ktu.offloadz.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {
  Order findByOrderId(int id);

  List<Order> findAll();
}
