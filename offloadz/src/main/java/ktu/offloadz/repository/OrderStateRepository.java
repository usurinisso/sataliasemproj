package ktu.offloadz.repository;

import ktu.offloadz.model.OrderState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderStateRepository extends JpaRepository<OrderState, Integer> {
  OrderState findByOrderStateId(int id);
}
