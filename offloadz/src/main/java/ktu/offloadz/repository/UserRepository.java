package ktu.offloadz.repository;

import ktu.offloadz.model.User;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;

import java.awt.print.Pageable;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
  User findByEmail(String email);
  User findById(int id);

  @Modifying
  @Query("delete from User t where t.regExpiryDate <= ?1 and t.active = 0")
  void deleteAllExpiredSince(Date now);
}
