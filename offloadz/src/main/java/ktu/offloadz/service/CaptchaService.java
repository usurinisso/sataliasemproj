package ktu.offloadz.service;

import ktu.offloadz.exceptions.ReCaptchaInvalidException;

public interface CaptchaService {
  void processResponse(final String response) throws ReCaptchaInvalidException;

  String getReCaptchaSite();

  String getReCaptchaSecret();
}
