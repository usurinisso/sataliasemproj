package ktu.offloadz.service;

import ktu.offloadz.model.Order;
import ktu.offloadz.model.User;

import java.util.List;

public interface OrderService {

  Order findById(int id);

  Order save(Order order);

  Order attachComment(User user, Order order, String message);

  Order updateState(Order order, int state);

  List<Order> filter(
      Integer id,
      Double minWeight,
      Double maxWeight,
      Double minHeight,
      Double maxHeight,
      Double minWidth,
      Double maxWidth,
      Double minLength,
      Double maxLength,
      String origin,
      String dest,
      Integer state);

  List<Order> findAll();

  List<Order> findSelectedDispatcher(String dispatcherName);
}
