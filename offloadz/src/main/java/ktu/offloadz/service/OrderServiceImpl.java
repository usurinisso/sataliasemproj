package ktu.offloadz.service;

import ktu.offloadz.model.Comment;
import ktu.offloadz.model.Order;
import ktu.offloadz.model.OrderState;
import ktu.offloadz.model.User;
import ktu.offloadz.repository.CommentRepository;
import ktu.offloadz.repository.OrderRepository;
import ktu.offloadz.repository.OrderStateRepository;
import ktu.offloadz.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class OrderServiceImpl implements OrderService {

  private final UserRepository userRepository;
  private final OrderRepository orderRepository;
  private final CommentRepository commentRepository;
  @Autowired private OrderStateRepository orderStateRepository;

  @Autowired
  public OrderServiceImpl(
      OrderRepository orderRepository,
      UserRepository userRepository,
      CommentRepository commentRepository) {
    this.orderRepository = orderRepository;
    this.userRepository = userRepository;
    this.commentRepository = commentRepository;
  }

  @Override
  public Order findById(int id) {
    return orderRepository.findByOrderId(id);
  }

  @Override
  public Order save(Order order) {
    return orderRepository.save(order);
  }

  @Override
  public Order attachComment(User user, Order order, String message) {
    Comment comment = new Comment();
    comment.setAuthor(user.getName() + " " + user.getLastName());
    comment.setMessage(message);
    commentRepository.save(comment);
    Set<Comment> orderComments = order.getOrderComment();
    orderComments.add(comment);
    order.setOrderComment(orderComments);
    return orderRepository.save(order);
  }

  @Override
  public Order updateState(Order order, int state) {
    OrderState orderState = orderStateRepository.findByOrderStateId(state);
    order.setOrderState(orderState);
    return orderRepository.save(order);
  }

  @Override
  public List<Order> findAll() {
    return orderRepository.findAll();
  }

  @Override
  public List<Order> findSelectedDispatcher(String dispatcherName) {
    List<Order> allOrders = findAll();
    List<Order> usable = new ArrayList<Order>();
    for (Order order : allOrders) {
      String name = order.getCreated_order();
      if (dispatcherName.equals(name)) usable.add(order);
    }
    return usable;
  }

  @Override
  public List<Order> filter(
      Integer id,
      Double minWeight,
      Double maxWeight,
      Double minHeight,
      Double maxHeight,
      Double minWidth,
      Double maxWidth,
      Double minLength,
      Double maxLength,
      String origin,
      String dest,
      Integer state) {
    List<Order> allOrders = findAll();
    List<Order> filteredOrders = new ArrayList<Order>();

    Stream<Order> orderStream = allOrders.stream();
    if (id != 0) {
      orderStream = orderStream.filter(s -> s.getOrderId() == id);
    }
    if (minWeight != 0.0) {
      orderStream = orderStream.filter(s -> s.getWeight() >= minWeight);
    }
    if (maxWeight != 0.0) {
      orderStream = orderStream.filter(s -> s.getWeight() <= maxWeight);
    }
    if (minHeight != 0.0) {
      orderStream = orderStream.filter(s -> s.getHeight() >= minHeight);
    }
    if (maxHeight != 0.0) {
      orderStream = orderStream.filter(s -> s.getHeight() <= maxHeight);
    }
    if (minWidth != 0.0) {
      orderStream = orderStream.filter(s -> s.getWidth() >= minWidth);
    }
    if (maxWidth != 0.0) {
      orderStream = orderStream.filter(s -> s.getWidth() <= maxWidth);
    }
    if (minLength != 0.0) {
      orderStream = orderStream.filter(s -> s.getLength() >= minLength);
    }
    if (maxLength != 0.0) {
      orderStream = orderStream.filter(s -> s.getLength() <= maxLength);
    }
    if (!origin.equals("")) {
      orderStream = orderStream.filter(s -> s.getOriginLocation().equals(origin));
    }
    if (!dest.equals("")) {
      orderStream = orderStream.filter(s -> s.getDestination().equals(dest));
    }
    if (state != 1) {
      orderStream = orderStream.filter(s -> s.getOrderId() == state);
    }
    filteredOrders = orderStream.collect(Collectors.toList());

    if (filteredOrders != null) {
      filteredOrders = filteredOrders.stream().distinct().collect(Collectors.toList());
    }
    return filteredOrders;
  }
}
