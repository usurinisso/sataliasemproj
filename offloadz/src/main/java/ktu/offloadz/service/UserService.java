package ktu.offloadz.service;

import ktu.offloadz.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserService {

  User findUserByEmail(String email);

  User saveUser(User user, String role);

  User editUser(User user, String role);

  void deleteUserById(int id);

  User updateUserStatus(User user, int value);

  User updateUserPassword(User user, String password);

  List<User> findAll();
  Page<User> findAll(Pageable pageable);
  User findUserById(int id);
}
