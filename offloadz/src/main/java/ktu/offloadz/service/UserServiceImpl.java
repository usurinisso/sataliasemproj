package ktu.offloadz.service;

import ktu.offloadz.model.Role;
import ktu.offloadz.model.User;
import ktu.offloadz.repository.RoleRepository;
import ktu.offloadz.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;
  private final RoleRepository roleRepository;
  private final BCryptPasswordEncoder bCryptPasswordEncoder;

  @Autowired
  public UserServiceImpl(
      UserRepository userRepository,
      RoleRepository roleRepository,
      BCryptPasswordEncoder bCryptPasswordEncoder) {
    this.userRepository = userRepository;
    this.roleRepository = roleRepository;
    this.bCryptPasswordEncoder = bCryptPasswordEncoder;
  }

  @Override
  public User findUserByEmail(String email) {
    return userRepository.findByEmail(email);
  }

  @Override
  public User saveUser(User user, String role) {
    user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
    user.setRegExpiryDate(user.calculateExpiryDate());
    Role userRole = roleRepository.findByRole(role);
    user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
    return userRepository.save(user);
  }

  @Override
  public User editUser(User user, String role) {
    Role userRole = roleRepository.findByRole(role);
    user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
    return userRepository.save(user);
  }

  @Override
  public void deleteUserById(int id) {
    User user = userRepository.findById(id);
    userRepository.delete(user);
  }

  @Override
  public User updateUserStatus(User user, int value) {
    user.setActive(value);
    return userRepository.save(user);
  }

  @Override
  public User updateUserPassword(User user, String password) {
    user.setPassword(bCryptPasswordEncoder.encode(password));
    return userRepository.save(user);
  }

  @Override
  public List<User> findAll() {
    return userRepository.findAll();
  }

  @Override
  public Page<User> findAll(Pageable pageable) {
    return userRepository.findAll(pageable);
  }

  @Override
  public User findUserById(int id) {
    return userRepository.findById(id);
  }
}
