package ktu.offloadz.service;

import ktu.offloadz.model.User;
import org.springframework.mail.SimpleMailMessage;

public interface VerificationEmailService {

  void sendConfirmationEmail(User user);

  void sendPasswordResetEmail(User user);

  void sendEmail(SimpleMailMessage email);
}
