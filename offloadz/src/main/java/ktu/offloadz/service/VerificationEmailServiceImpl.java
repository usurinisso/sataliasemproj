package ktu.offloadz.service;

import ktu.offloadz.model.User;
import ktu.offloadz.model.VerificationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class VerificationEmailServiceImpl implements VerificationEmailService {

  private final JavaMailSender mailSender;

  @Autowired
  public VerificationEmailServiceImpl(JavaMailSender mailSender) {
    this.mailSender = mailSender;
  }

  @Autowired private VerificationTokenService verificationTokenService;

  @Async
  @Override
  public void sendEmail(SimpleMailMessage email) {
    mailSender.send(email);
  }

  @Override
  public void sendConfirmationEmail(User user) {

    VerificationToken verificationToken = new VerificationToken(user);
    verificationTokenService.saveVerificationToken(verificationToken);

    SimpleMailMessage mailMessage = new SimpleMailMessage();
    mailMessage.setTo(user.getEmail());
    mailMessage.setSubject("Complete Registration!");
    mailMessage.setFrom("example@gmail.com");
    mailMessage.setText(
        "To confirm your account, please click here : "
            + "http://localhost:8081/confirm-account?token="
            + verificationToken.getVerificationToken());

    sendEmail(mailMessage);
  }

  @Override
  public void sendPasswordResetEmail(User user) {

    VerificationToken confirmationToken = new VerificationToken(user);

    verificationTokenService.saveVerificationToken(confirmationToken);

    SimpleMailMessage mailMessage = new SimpleMailMessage();
    mailMessage.setTo(user.getEmail());
    mailMessage.setSubject("Complete Password Reset!");
    mailMessage.setFrom("Offloadz123@gmail.com");
    mailMessage.setText(
        "To complete the password reset process, please click here: "
            + "http://localhost:8081/confirm-reset?token="
            + confirmationToken.getVerificationToken());

    sendEmail(mailMessage);
  }
}
