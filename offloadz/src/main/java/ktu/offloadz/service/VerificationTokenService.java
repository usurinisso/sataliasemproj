package ktu.offloadz.service;

import ktu.offloadz.model.VerificationToken;

public interface VerificationTokenService {
  VerificationToken saveVerificationToken(VerificationToken verificationToken);

  VerificationToken findByVerificationToken(String verificationToken);
}
