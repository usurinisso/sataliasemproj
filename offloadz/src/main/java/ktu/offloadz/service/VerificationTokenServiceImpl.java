package ktu.offloadz.service;

import ktu.offloadz.model.VerificationToken;
import ktu.offloadz.repository.VerificationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VerificationTokenServiceImpl implements VerificationTokenService {

  private final VerificationTokenRepository verificationTokenRepository;

  @Autowired
  public VerificationTokenServiceImpl(VerificationTokenRepository verificationTokenRepository) {
    this.verificationTokenRepository = verificationTokenRepository;
  }

  @Override
  public VerificationToken findByVerificationToken(String verificationToken) {
    return verificationTokenRepository.findByVerificationToken(verificationToken);
  }

  @Override
  public VerificationToken saveVerificationToken(VerificationToken verificationToken) {
    return verificationTokenRepository.save(verificationToken);
  }
}
