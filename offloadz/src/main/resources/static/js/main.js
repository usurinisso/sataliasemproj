$(document).ready(function () {

    $('.addButton').on('click', function (event) {
        event.preventDefault();
        var href = this.getAttribute('href');
        $('.userForm #name').val('');
        $('.userForm #lastName').val('');
        $('.userForm #email').val('');
        $('.userForm #password').val('');
        $('.userForm #userAddModal').modal();
    });

    $('.editButton').on('click', function (event) {
        event.preventDefault();
        var href = this.getAttribute('href');
        $.get(href, function (user, status) {
            $('.userForm2 #id').val(user.id);
            $('.userForm2 #name2').val(user.name);
            $('.userForm2 #lastName2').val(user.lastName);
            $('.userForm2 #email2').val(user.email);
            $('.userForm2 #password').val(user.password);
        });
        $('.userForm2 #userEditModal').modal();
    });

    $('.deleteButton').on('click', function (event) {
        event.preventDefault();
        var href = $(this).attr('href');
        $('#userDeleteModal #delRef').attr('href', href);
        $('#userDeleteModal').modal();
    });
});