package ktu.offloadz.repository;

import ktu.offloadz.model.Order;
import ktu.offloadz.model.OrderState;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class OrderRepositoryTest {
  @Autowired private TestEntityManager entityManager;

  @Autowired private OrderRepository orderRepository;

  @Test
  public void whenFindByOrderId_thenReturnOrder() {
    // Given
    Order testOrder = new Order();
    testOrder.setWidth(2.5);
    testOrder.setHeight(3.4);
    testOrder.setWeight(2.0);
    testOrder.setLength(5.0);
    testOrder.setDestination("Utena");
    testOrder.setOriginLocation("Kaunas");
    testOrder.setOrderState(new OrderState(1, "None"));

    entityManager.persist(testOrder);
    entityManager.flush();

    // When
    Order foundOrder = orderRepository.findByOrderId(testOrder.getOrderId());

    // Then
    assertThat(foundOrder).isEqualTo(testOrder);
  }

  @Test
  public void whenFindAll_thenReturnAllOrders() {
    // Given
    orderRepository.deleteAll();
    Order testOrder1 = new Order();
    testOrder1.setWidth(2.5);
    testOrder1.setHeight(3.4);
    testOrder1.setWeight(2.0);
    testOrder1.setLength(5.0);
    testOrder1.setDestination("Utena");
    testOrder1.setOriginLocation("Kaunas");
    testOrder1.setOrderState(new OrderState(1, "None"));

    Order testOrder2 = new Order();
    testOrder2.setWidth(12.8);
    testOrder2.setHeight(7.4);
    testOrder2.setWeight(1.15);
    testOrder2.setLength(6.0);
    testOrder2.setDestination("Klaipėda");
    testOrder2.setOriginLocation("Šiauliai");
    testOrder2.setOrderState(new OrderState(1, "None"));

    List<Order> orderList = new ArrayList<>();
    orderList.add(testOrder1);
    orderList.add(testOrder2);

    entityManager.persist(testOrder1);
    entityManager.persist(testOrder2);
    entityManager.flush();

    // When
    List<Order> orderListFindAll;
    orderListFindAll = orderRepository.findAll();

    // Then
    assertThat(orderListFindAll).containsExactlyInAnyOrderElementsOf(orderList);
  }
}
