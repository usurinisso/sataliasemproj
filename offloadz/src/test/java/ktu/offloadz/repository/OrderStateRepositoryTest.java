package ktu.offloadz.repository;

import ktu.offloadz.model.OrderState;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class OrderStateRepositoryTest {
  @Autowired private TestEntityManager entityManager;

  @Autowired private OrderStateRepository orderStateRepository;

  @Test
  public void whenFindByOrderStateId_thenReturnOrderState() {
    // Given
    OrderState testOrderState = new OrderState();
    testOrderState.setState("In transit");

    entityManager.persist(testOrderState);
    entityManager.flush();

    // When
    OrderState foundOrderState =
        orderStateRepository.findByOrderStateId(testOrderState.getOrderStateId());

    // Then
    assertThat(testOrderState.getState()).isEqualTo(foundOrderState.getState());
  }
}
