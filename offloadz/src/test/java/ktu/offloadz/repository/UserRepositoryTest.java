package ktu.offloadz.repository;

import ktu.offloadz.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {
  @Autowired private TestEntityManager entityManager;

  @Autowired private UserRepository userRepository;

  @Test
  public void whenFindByEmail_thenReturnUser() {
    // Given
    User testUser = new User();
    testUser.setEmail("jpetraitis@test.test");
    testUser.setName("Jonas");
    testUser.setLastName("Petraitis");
    testUser.setPassword("Jonas12petr");

    entityManager.persist(testUser);
    entityManager.flush();

    // When
    User foundUser = userRepository.findByEmail(testUser.getEmail());

    // Then
    assertThat(foundUser.getName()).isEqualTo(testUser.getName());
  }

  @Test
  public void whenFindById_thenReturnUser() {
    // Given
    User testUser = new User();
    testUser.setEmail("jpetraitis@test.test");
    testUser.setName("Jonas");
    testUser.setLastName("Petraitis");
    testUser.setPassword("Jonas12petr");

    entityManager.persist(testUser);
    entityManager.flush();

    // When
    User foundUser = userRepository.findById(testUser.getId());

    // Then
    assertThat(foundUser.getName()).isEqualTo(testUser.getName());
  }

  @Test
  public void whenDeleteAllExpiredSince_thenDeleteAllExpiredUsers() {
    ZoneId defaultZoneId = ZoneId.systemDefault();

    // creating the instance of LocalDate using the day, month, year info
    LocalDate localDate = LocalDate.of(2020, 3, 23);
    LocalDate localDateNow = LocalDate.of(2020, 4, 23);

    Date date = Date.from(localDate.atStartOfDay(defaultZoneId).toInstant());
    Date now = Date.from(localDateNow.atStartOfDay(defaultZoneId).toInstant());

    // Given
    User testUser = new User();
    testUser.setEmail("jpetraitis@test.test");
    testUser.setName("Jonas");
    testUser.setLastName("Petraitis");
    testUser.setPassword("Jonas12petr");
    testUser.setActive(0);
    testUser.setRegExpiryDate(date);

    entityManager.persist(testUser);
    entityManager.flush();

    userRepository.deleteAllExpiredSince(now);

    // Then
    assertThat(userRepository.findByEmail(testUser.getEmail())).isEqualTo(null);
  }
}
