package ktu.offloadz.repository;

import ktu.offloadz.model.User;
import ktu.offloadz.model.VerificationToken;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class VerificationTokenRepositoryTest {
  @Autowired private TestEntityManager entityManager;

  @Autowired private VerificationTokenRepository verificationTokenRepository;

  @Autowired private UserRepository userRepository;

  @Test
  public void whenFindByVerificationToken_thenReturnVerificationToken() {
    // Given
    VerificationToken testVerificationToken = new VerificationToken();

    User testUser = new User();
    testUser.setEmail("jpetraitis@test.test");
    testUser.setName("Jonas");
    testUser.setLastName("Petraitis");
    testUser.setPassword("Jonas12petr");
    entityManager.persist(testUser);

    testVerificationToken.setUser(userRepository.findByEmail("jpetraitis@test.test"));
    testVerificationToken.setVerificationToken("J4ger854WE98cdw");

    entityManager.persist(testVerificationToken);
    entityManager.flush();

    // When
    VerificationToken foundVerificationToken =
        verificationTokenRepository.findByVerificationToken(
            testVerificationToken.getVerificationToken());

    // Then
    assertThat(foundVerificationToken.getVerificationToken())
        .isEqualTo(testVerificationToken.getVerificationToken());
  }

  @Test
  public void whenDeleteAllExpiredSince_thenDeleteAllExpiredVerificationTokens() {
    ZoneId defaultZoneId = ZoneId.systemDefault();

    // creating the instance of LocalDate using the day, month, year info
    LocalDate localDate = LocalDate.of(2020, 3, 23);
    LocalDate localDateNow = LocalDate.of(2020, 4, 23);

    Date date = Date.from(localDate.atStartOfDay(defaultZoneId).toInstant());
    Date now = Date.from(localDateNow.atStartOfDay(defaultZoneId).toInstant());

    VerificationToken testVerificationToken = new VerificationToken();

    User testUser = new User();
    testUser.setEmail("jpetraitis@test.test");
    testUser.setName("Jonas");
    testUser.setLastName("Petraitis");
    testUser.setPassword("Jonas12petr");
    entityManager.persist(testUser);

    testVerificationToken.setUser(userRepository.findByEmail("jpetraitis@test.test"));
    testVerificationToken.setVerificationToken("J4ger854WE98cdw");
    testVerificationToken.setExpiryDate(date);

    entityManager.persist(testVerificationToken);
    entityManager.flush();

    verificationTokenRepository.deleteAllExpiredSince(now);

    // Then
    assertThat(
            verificationTokenRepository.findByVerificationToken(
                testVerificationToken.getVerificationToken()))
        .isEqualTo(null);
  }
}
