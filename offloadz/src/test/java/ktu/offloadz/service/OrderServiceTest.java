package ktu.offloadz.service;

import ktu.offloadz.model.Comment;
import ktu.offloadz.model.Order;
import ktu.offloadz.model.OrderState;
import ktu.offloadz.model.User;
import ktu.offloadz.repository.CommentRepository;
import ktu.offloadz.repository.OrderRepository;
import ktu.offloadz.repository.OrderStateRepository;
import ktu.offloadz.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;

@ExtendWith(MockitoExtension.class)
@DataJpaTest
class OrderServiceTest {

  @InjectMocks
  private OrderServiceImpl orderService;

  @Mock
  private OrderRepository orderRepository;

  @Mock
  private UserRepository userRepository;

  @Mock
  private CommentRepository commentRepository;

  @Mock
  private OrderStateRepository orderStateRepository;

  @Autowired
  private TestEntityManager entityManager;

  private Order testOrder;

  private Order testOrder2;

  private Order testOrder3;

  private User testUser;

  private OrderState testOrderState;

  @BeforeEach
  public void setUp() {
    testOrder = new Order();
    testOrder.setWidth(2.5);
    testOrder.setHeight(3.4);
    testOrder.setWeight(2.0);
    testOrder.setLength(5.0);
    testOrder.setDestination("Utena");
    testOrder.setOriginLocation("Kaunas");
    testOrder.setOrderState(new OrderState(1, "None"));
    testOrder.setOrderComment(new HashSet<Comment>());
    testOrder.setCreated_order("Jonas");

    testOrder2 = new Order();
    testOrder2.setWidth(9.1);
    testOrder2.setHeight(4.6);
    testOrder2.setWeight(3.8);
    testOrder2.setLength(12.8);
    testOrder2.setDestination("Vilnius");
    testOrder2.setOriginLocation("Klaipėda");
    testOrder2.setOrderState(new OrderState(1, "None"));
    testOrder2.setOrderComment(new HashSet<Comment>());
    testOrder2.setCreated_order("Mykolas");

    testOrder3 = new Order();
    testOrder3.setWidth(3.1);
    testOrder3.setHeight(8.4);
    testOrder3.setWeight(2.1);
    testOrder3.setLength(4.0);
    testOrder3.setDestination("Tauragė");
    testOrder3.setOriginLocation("Šiauliai");
    testOrder3.setOrderState(new OrderState(1, "None"));
    testOrder3.setOrderComment(new HashSet<Comment>());
    testOrder3.setCreated_order("Domantas");

    testUser = new User();
    testUser.setEmail("jpetraitis@test.test");
    testUser.setName("Jonas");
    testUser.setLastName("Petraitis");
    testUser.setPassword("Jonas12petr");

    entityManager.persist(testOrder);
    entityManager.persist(testOrder2);
    entityManager.persist(testOrder3);
    entityManager.persist(testUser);
    entityManager.flush();

    List<Order> testAllOrders = new ArrayList<Order>();
    testAllOrders.add(testOrder);
    testAllOrders.add(testOrder2);
    testAllOrders.add(testOrder3);

    Mockito.when(orderService.findAll()).thenReturn(testAllOrders);
    Mockito.when(orderService.findById(anyInt())).thenReturn(testOrder);
  }

  @Test
  void whenAttachComment_thenAttachCommentToOrder() {
    String testMessage = "Test comment.";
    orderService.attachComment(testUser, testOrder, testMessage);
    String mockMessage =
            orderService
                    .findById(testOrder.getOrderId())
                    .getOrderComment()
                    .iterator()
                    .next()
                    .getMessage();
    assertThat(mockMessage).isEqualTo(testMessage);
  }

  @Test
  void whenUpdateState_thenUpdateOrderState() {
    int testState = 4;
    String testStateMessage = "Delivered";
    testOrderState = new OrderState(testState, testStateMessage);
    Mockito.when(orderStateRepository.findByOrderStateId(anyInt())).thenReturn(testOrderState);
    orderService.updateState(testOrder, testState);
    assertThat(testOrder.getOrderState().getState()).isEqualTo(testStateMessage);
  }

  @Test
  void whenFilter_thenFilterOrdersByAllEnteredCriteria() {
    List<Order> testFiltered = new ArrayList<Order>();
    testFiltered =
            orderService.filter(0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 5.0, 13.0, "", "Vilnius", 1);
    assertThat(testFiltered).containsOnly(testOrder2);
  }

  @Test
  void whenFindSelectedDispatcher_thenFindAllOrdersForASpecificDispatcher() {
    String testDispatcherName = testOrder3.getCreated_order();
    List<Order> testDispatcherList = new ArrayList<Order>();
    testDispatcherList = orderService.findSelectedDispatcher(testDispatcherName);
    assertThat(testDispatcherList).containsOnly(testOrder3);
  }
}
