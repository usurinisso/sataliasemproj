package ktu.offloadz.service;

import ktu.offloadz.model.Role;
import ktu.offloadz.model.User;
import ktu.offloadz.repository.RoleRepository;
import ktu.offloadz.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;


@ExtendWith(MockitoExtension.class)
@DataJpaTest
class UserServiceTest {

    @InjectMocks
    public UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private TestEntityManager entityManager;

    public Role testRole;

    public User testUser;

    @BeforeEach
    public void setUp() {
        //Mockito.when(userRepository.findById(anyInt())).thenReturn(testUser);
    }

    @Test
    void saveUser() {
        testRole = new Role();
        testRole.setId(4);
        testRole.setRole("TEST_ROLE");

        testUser = new User();
        testUser.setEmail("jpetraitis@test.test");
        testUser.setName("Jonas");
        testUser.setLastName("Petraitis");
        testUser.setPassword("Jonas12petr");

        entityManager.persist(testUser);
        entityManager.flush();

        String testEncodedPassword = "mockEncode";


        // Though these two stubs do work here inside of this class,
        // they do not work inside of userService.saveUser() for some reason.
        Mockito.when(bCryptPasswordEncoder.encode(anyString())).thenReturn(testEncodedPassword);
        Mockito.when(roleRepository.findByRole(anyString())).thenReturn(testRole);

        userService.saveUser(testUser, "TEST_ROLE");
        assertThat(testRole.getRole()).isEqualTo(testUser.getRole());
    }

    @Test
    void editUser() {
    }

    @Test
    void deleteUserById() {
    }

    @Test
    void updateUserStatus() {
    }

    @Test
    void updateUserPassword() {
    }
}